---
layout: page
permalink: /about/
title: about 
tagline: 
tags: [about]
modified: 1-14-2014
image:
  feature: feat/circuit.jpg
---

When I rolled out of Philadelphia, people asked me to start a blog.  I ended up
making a cleaned up version of [my site](http://kfring.com)[^1]  

## What I'm all about:

* I'm a pretty big nerd.
* I'm self employed through [my consulting business](http://eversonit.com)[^2]. 
* I work in IT.
* I have an older sibling.
* Kinesthetically challenged.
* I read a lot.
* I *love* puns.
* I am not a great writter.

[Road Trip Archives]({{ site.url }}/roadtrip)

[^1]: Built with Jekyll, hosted on Amazon s3/CloudFront.

[^2]: Yeah, I know. Totally legit looking. If you want to hire me, I'd love to talk.
