---
layout: page
permalink: /thelist/
title: The List 
tagline: Things left done & undone.
tags: [lists, goals]
modified: 1-11-2014
image:
  feature: feat/haycock-diabase.jpg
  credit: Kyle Fring                                                             
  creditlink: http://kfring.com
---


* Boulder <b>V5</b>, <b>V6</b>, <b>V7</b>, V8, V9, V10, V11.
* Onsite 5.12
* Lead 5.9 on gear
* Finish a Half Marathon
* Ride a bike across a City
* Ride a bike for 40 miles in a day
* Buy a lottery ticket
* Gamble $100 in one shot at a Casino
* Motorcycle License
* Foot race a goat
* Drive a Ferrari at 170+mph
* <b>Spend less than $500 in one month</b>
* Cook an excellent dinner for 5+ people on my own
* Visit Morocco, Iceland, China, Kathmandu, Tibet, Thailand, India
* Stay in each U.S. State for at least 1 day (<b>35</b>/50)
* Build a pair of sun/glasses
* Build a guitar
* Play and sing for an audience
* Take one really great photograph
* Write at least 5 sentences every day for one year
* Read for half an hour every day for one year
* Run for 15 minutes every day for one year
* Spend 48 hours without speaking/hearing/seeing
* Walk slowly away from an explosion
* Curl 5 reps of 25 @ 50lbs
* Fire an entire clip through an automatic rifle
* <b>Save someones life</b>[^1]
* <b>Save my own life</b>[^2]
* Skydive
* BASE Jump




[^1]: Summer of 2002 while life guarding.
[^2]: I think this one is a little silly.  For sure I've been in situations where the outcome would be fatal and lived. Although not smoking probably counts the most.
