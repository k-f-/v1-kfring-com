---
layout: post
title: "Road Trip: Ohio and West Virginia"
modified: 2014-01-14
category: roadtrip
tags: [road trip, climbing, bouldering, dirtbag, travel]
description: "The best Sandstone on Earth. When it's not wet or frozen."
image:
  feature: feat/nrg-sandstone1.jpg
  credit: Kyle Fring
  creditlink: http://kfring.com
---

## Ohio

Ohio *still* sucks[^1].  That is all.

## West Virginia

<figure>
  <img src="/images/posts/nrg-04.jpg">
  <figcaption>View of the rig and the bridge as seen from the Bridge Boulders
  area.</figcaption>
</figure>

### Bouldering

[Nuttall Sandstone](http://www.wvgs.wvnet.edu/www/geology/geoles01.htm) is
bullet hard and lacks crazy features found in softer stone.  Reportedly it's
conducive to hard bouldering[^3].

I didn't really get all excited about the river bed bouldering in the New
River Gorge[^2] until someone pointed out that these boulders are really,
really, epically old.  The New River system is probably second only to
the Nile in terms of age.  The boulders exposed by damming of the river have been
underwater for a long, long time.

The locals are doing an awesome job in the development of bouldering areas.
Cleaning, making trails, figuring out circuits, etc.  A new guide book is in the
works and there are thousands of problems to include.

To circuit the NRG bouldering you have to be willing to drive and hike in
between all of the areas, possibly visiting 3 or four spots in a day.  Once you
start to get an idea of how the loop road that drops you down into the gorge
works, it's a lot less irritating.

The New in the winter is beautiful.  I can't imagine how good it must be in the
fall and spring.

### Weather

West Virginia weather is notoriously unpredictable.  I got hammered with record
lows, snow and rain.  I only got 3 sessions outside of the 7 days I was there. I
did however have two great night sessions with mild evening temps.

<figure class='half'>
  <a href="/images/posts/nrg-01.jpg"><img src="/images/posts/nrg-01.jpg"></a>
  <a href="/images/posts/nrg-02.jpg"><img src="/images/posts/nrg-02.jpg"></a>
  <figcaption>Two from Hawk's Nest.</figcaption>
</figure>


[^1]: It was really nice however, to see an old friend.
[^2]: known locally as the Dries.
[^3]: Not that I boulder hard.

