---
layout: post
title: "Road Trip: Tennessee and Georgia"
modified: 2014-02-11
category: roadtrip
tags: [road trip, climbing, bouldering, dirtbag, travel]
description: "Land of the Cherokee, Shawnee & Slopers."
image:
  feature: feat/smokey-01.jpg
  credit: Myself
  creditlink: http://kfring.com
---

### Smokey Mountains 

Spent just one night in Smokey Mountain National Park after leaving West
Virginia.  Did some trail running early the next morning.  It's a pleasant
enough place, but I wasn't blown away.  I'm sure the back-country contains the
most interesting bits because I sure as hell didn't see it on my car friendly
tour.

There is a city just south of the park, I forget the name but it essentially
feels like a tiny version of Las Vegas.  Wax Museum, Talking pigs, strippers.
Pretty weird.

### Stone Fort (aka Little Rock City)

The strangest setting for climbing ever.  Registering at a golf club
house is legit weird.  However, this is the most casual approach I've ever done.
I took very few pictures, mostly of dogs I found...while the climbing is technical and *good*
it just didn't get my all jazzed up.  So be it.

On the weekends this place is more packed than a brothel on shore leave.  Gross.
Wait your turn to climb outside.  Like. In. A. Line.  Oh, you sneezed? Missed
your chance.  Felt like trying to order a cheese steak at Geno's or Pat's in
Philadelphia.  No thanks.

Ticked some things, nothing particularly proud.

<figure>
  <img src="/images/posts/lrc-shield.jpg">
  <figcaption>A shot of --The Shield v12</figcaption>
</figure>



### Rocktown.

**JEEEESSSSUUUUUSSSS!!!!!!** I don't ever want to leave, but I will this coming Thursday (2/6/2014).  Been here
two weeks.  Cut into HP40 for it.  It's that good.  I even bought a guide book,
which I would suggest for any area if you're on your own and actually want to
have fun.

I've had two really good weeks of climbing and weather with a bunch of ticks I'm
really happy about.  I'm hoping this rain and mank clears out so I can get on a
few more things before I have to bail for HP40 en route to Hueco.

#### Cold. Really.

Good weather usually means 50 and sunny in the South East during winter.  I
instead received highs of 25 with wind and partly cloudy. *However*, it didn't rain
for almost two weeks straight.  I'll take that 100% of the time. Negative temps
  at night is kind of silly, but I stayed when other less bearded men headed for
  the comfort of the Key West Inn in LaFayette.

<figure class="half">
	<a href="/images/posts/rocktown-cold01.jpg"><img
    src="/images/posts/rocktown-cold01.jpg"></a>
	<a href="/images/posts/rocktown-cold02.jpg"><img
    src="/images/posts/rocktown-cold02.jpg"></a>
	<figcaption>Yeah, it was cold.</figcaption>
</figure>

#### it does the climbs

Love the climbing here. You can have five star whatever you want.  It does get
busy on the weekends but since it's spread out a bit you can be as anti-social
as you like.  I now understand why people have a raging hard on for
south-east-slopers -- It's **wild**.  The stone is also mostly skin friendly.
The amount of mileage you can get done without bleeding, tearing or other aweful
outcomes is exceptional.  I'd move here for this place.

<figure class="half">
	<a href="/images/posts/rocktown-littlebad.jpg"><img
    src="/images/posts/rocktown-littlebad.jpg"></a>
        <a href="/images/posts/rocktown-flapper.jpg"><img
    src="/images/posts/rocktown-flapper.jpg"></a>
	<figcaption>The rock won.  You slopey bastard.</figcaption>
</figure>

### Chattanooga

<figure>
  <a href="/images/posts/chatt_snow.jpg"><img
  src="/images/posts/chatt_snow.jpg"></a>
  <figcaption>Yeah, I got snowed on while down here. Chattanooga totally shut
  down.</figcaption>
</figure>

#### The City

The city is small, but not overly so.  According to some drunks at a bar it is
*hopping* in the Summer months.  I of course saw none of that so this jury is
still out on the night life.  Even so, by Philadelphia standards the city is
super safe.  I had zero issues wandering around like a true vagrant well into
the evening.  If you're a climber, or even just into the Outdoors it's a very
centrally located city.  Whatever your poison you'll probably find it within an
hours drive.

Spots in town I liked:

1. [Lupi Pizza](http://lupi.com/)
2. [Mean Mug Coffe](http://www.meanmugcoffee.com) *blazing free wifi*
3. Butterfly exhibit at the [Aquarium](http://www.tennesseeaquarium.org)

#### The Crash Pad

Chattanooga is home to an excellent climber run hostel, [The Crash
Pad](www.crashpadchattanooga.com).  The staff is super psyched you're there.  I
think it's best feature by far is the convenience of doing laundry on the
premises.  It's not cheap, but I didn't get bed bugs or any other kind of nasty.
They even let me use the showers which was welcome after 9 days of Wal-Mart
camping. They also have all you can eat breakfast included in the price.  I
averaged 2 nutella sandwiches, 4 cups of coffee and 3 eggs on my stays there.

#### Friends

Made some. I think.
