---
layout: post
title: "Road Trip: Notes"
modified: 2014-02-11
category: roadtrip
tags: [road trip, climbing, bouldering, dirtbag, travel]
description: "betas."
image:
  feature: feat/chippewa.jpg
  credit: Craig Fring
  creditlink: http://flickr.com/photos/kfring/
---

## Notes

* 14 Days is about as long as I'd like to go without a shower.
* Gear gets annihilated being used every day.
* Avoid Indian Reservations. People be bored.
* 14 Hours of driving seems to be my personal limit.
* Caffeinated Soda can be a life saver.

## Thing I wish I thought of beforehand.

1. Boxed Wine or Sangria[^1].  
2. Thick **work** socks, two pair. Rotate.
3. ExOfficio Underwear, two pair. Rotate.
4. Running Leggings, not just thermals. two pair. Rotate.
5. Non-cotton based pants. Moisture sucks.
6. Kitchen can be condensed to one bin.
 
## Things I'm not using enough.

1. Guitar. **shipped home**
2. Running Shoes. *too cold*

## Expenses

|---
| **Date** | **Item** | **Price** | **Note/MPG?**
| 1/1/2014 | Gasoline | $43.60 | Fill Tank
| 1/1/2014 | Americano | $3.40
| 1/1/2014 | Gasoline | $43.07 | 20.64
| 1/3/2014 | Food/Dinner | $42.00
| 1/3/2014 | Gasoline | $37.72 | 23.59
| 1/5/2014 | Gasoline | $32.11 | 16.93
| 1/9/2014 | Food/Rent | $40 | friends place
| 1/10/2014 | lantern mantels | $9
| 1/10/2014 | atlas/tarp/ratchet straps | $30
| 1/10/2014 | ATM | $100
| 1/10/2014 | Groceries | $44 | friends place
| 1/12/2014 | Gasoline | $32.05 | 19.73
| 1/12/2014 | Gasoline | $19.26 | 21.67
| 1/13/2014 | Gasoline | $35.27 | 22.43
| 1/14/2014 | Food/Coffee/Wifi | $8
| 1/14/2014 | LRC Fee(s) | $20
| 1/14/2014 | Gasoline | $39.36 | 18.9
| 1/15/2014 | night on the town  | $35
| 1/16/2014 | hostel/laundry | $32
| 1/18/2014 | Rocktown GORP annual | $24
| 1/18/2014 | groceries | $45
| 1/18/2014 | Gasoline | $38.19 | 17
| 1/28/2014 | rest/snowmaggedon | $100
| 1/28/2014 | Gasoline | $35 | 20
| 2/02/2014 | New Cell Phone | $110 | rutroh
| 2/02/2014 | Beers | $20 | need to stop doing this
| 2/03/2014 | tape | $4 | my skinnnnnn
| 2/09/2014  | HP40 Fee | $15
| 2/10/2014 | Gasoline | $34.28 | 23.8
| 2/11/2014 | Gasoline (No Ethanol) | $36 | 25


[^1]: Thanks Kirk.
