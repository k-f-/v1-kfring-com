---
layout: post
title: "Road Trip: Photos - LRC & Rocktown"
<!--- modified: 2014-02-11 -->
category: roadtrip
tags:
description: "some artsy junk."
image:
  feature: feat/se.jpg
  credit: Myself 
  creditlink: http://kfring.com
---

## Shots from Stonefort & Rocktown

<figure>
	<a href="/images/posts/se-01.jpg"><img src="/images/posts/se-01.jpg"></a>
    <figcaption>the sheild @ lrc</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-02.jpg"><img src="/images/posts/se-02.jpg"></a>
    <figcaption>the orb @ rocktown</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-03.jpg"><img src="/images/posts/se-03.jpg"></a>
    <figcaption>a four star dyno. No joke.</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-04.jpg"><img src="/images/posts/se-04.jpg"></a>
    <figcaption>iron ring on Golden Shower.</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-05.jpg"><img src="/images/posts/se-05.jpg"></a>
    <figcaption>the Golden Shower side-pull. It feels so niiiice.</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-06.jpg"><img src="/images/posts/se-06.jpg"></a>
    <figcaption>purrddy crimp on Golden Shower</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-07.jpg"><img src="/images/posts/se-07.jpg"></a>
    <figcaption>get scooped.</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-08.jpg"><img src="/images/posts/se-08.jpg"></a>
    <figcaption>how does this happen?</figcaption>
</figure>

<figure>
	<a href="/images/posts/se-09.jpg"><img src="/images/posts/se-09.jpg"></a>
    <figcaption>world class bouldering #choss all to myself.</figcaption>
</figure>
