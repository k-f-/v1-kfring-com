---
layout: post
title: "Road Trip: The Pork Chop Express"
modified: 2014-01-10
category: roadtrip
tags: [road trip, climbing, bouldering, dirtbag, travel]
description: "Jack Burton is a bawse."
image:
  feature: feat/roadtrip.jpg
  credit: Google Maps                                                                     
  creditlink: http://mapsengine.google.com  
---

<figure>
    <img src="/images/posts/element-01-01.jpg">
</figure>

## Big is beautiful.

I've always liked projects.  Making this pig something I'd be O.K. with while scuming my way across the southern part of America was a good time.  

### Sleeping Platform

Having an elevated, folding sleeping area was a necessity.  I'm pretty tall, and
not being able to stretch out comfortably would make me miserable.

#### Design considerations

I ended up designing a 60/40 split for the platform.  This increases the
modularity of the entire platform for storage. With one sides removal, it is
possible to stow tall objects like bikes while maintaining a large sleeping
area.  The base is supported by a front facing wall and a rib that runs the
length of the platform.  This rib is locked into the floor by u-bolts.

The front seats in the Element lie completely flat at roughly 14.5'' from
the interior floor.  The sheets of 3/4'' plywood I used for construction put the
entire platform about 15'' high.

<figure>
	<img src="/images/posts/element-02.jpg">
    <figcaption>The end result in full bed mode.</figcaption>
</figure>


#### Construction

I used 3/4'' ply, 1x2's and 2x4's where needed.  The basic unpainted, un-carpeted
build took about 16 hours with two sets of hands[^1].  Something I didn't
discover till it was all built was that with the front seats up and the leg
portion of the bed resting on them you get a sweet lounge chair.

I also made a new space tire cover to free up all the space under-neath.  I put
things I want to stay especially safe in there.
Cutting the board length-wise gives access to either side with the platform
installed.

<figure class="third">
	<a href="/images/posts/element-05.jpg"><img src="/images/posts/element-05.jpg"></a>
	<a href="/images/posts/element-04.jpg"><img src="/images/posts/element-04.jpg"></a>
	<a href="/images/posts/element-03.jpg"><img src="/images/posts/element-03.jpg"></a>	
    <figcaption>Construction and test fitting.</figcaption>
</figure>

<figure class="third">
	<a href="/images/posts/element-11.jpg"><img src="/images/posts/element-11.jpg"></a>
	<a href="/images/posts/element-10.jpg"><img src="/images/posts/element-10.jpg"></a>
	<a href="/images/posts/element-09.jpg"><img src="/images/posts/element-09.jpg"></a>	
    <figcaption>Small side, Retaining wall, large side.</figcaption>
</figure>

<figure class="third">
	<a href="/images/posts/element-08.jpg"><img src="/images/posts/element-08.jpg"></a>
	<a href="/images/posts/element-07.jpg"><img src="/images/posts/element-07.jpg"></a>
	<a href="/images/posts/element-06.jpg"><img src="/images/posts/element-06.jpg"></a>	
    <figcaption>Spine, Spare tire cover, all painted flat black.</figcaption>
</figure>

### Roof Rack

The roof rack I installed is from Yakima, sourced mostly from Craigslist. 58''
bars seemed to work best.  Also, you can get the Yakima Basket Case under the
bars, so I did that and secured the spare tire.

The roof-box is by far the best installed option.  I've got tons of useless crap
up there.  Retail for the entire setup is north of $900.  I spent $350, so
I'd recommend trolling ebay and craigslist to save quite a bit of scrilla.

<figure class="half">
    <a href="/images/posts/element-packed.jpg"><img
  src="/images/posts/element-packed.jpg"></a>
  <a href="/images/posts/element-ohio.jpg"><img src="/images/posts/element-ohio.jpg"></a>
  <figcaption>Showing the roof setup</figcaption>
</figure>

[^1]: Thanks, Dad.



