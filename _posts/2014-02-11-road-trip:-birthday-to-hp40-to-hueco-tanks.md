---
layout: post
title: "Road Trip: Birthday to HP40 to Hueco Tanks."
<!--- modified: 2014-02-11 -->
category: roadtrip
tags: [road trip, climbing, bouldering, dirtbag, travel]
description: "Getting so old."
image:
  feature: feat/rockcrush.jpg
  credit: 
  creditlink: 
---

## Heppy Birfdey tew meeee

Boom. 31. There must be some mistake.  I don't feel that old and I sure as hell
don't have things as figured out as my twelve yearold self would have expected.

I spent the day by myself at the camp ground at Rocktown, Georgia.  It's not
that I couldn't have gone and spent some time with some friends I had made in
Chattanooga, but instead I wanted to sit and read.  Besides: it was raining,
driving all over costs money, and frankly I wanted some solitude.

<figure class="third">
	<a href="/images/posts/bday-01.jpg"><img src="/images/posts/bday-01.jpg"></a>
	<a href="/images/posts/bday-02.jpg"><img src="/images/posts/bday-02.jpg"></a>
	<a href="/images/posts/bday-03.jpg"><img src="/images/posts/bday-03.jpg"></a>	
    <figcaption>bday photogs.</figcaption>
</figure>

## End of Rocktown

I capped my three week stay at Rocktown by finishing a few projects.  Lee & Nic
rolled down from West Virginia for two days.  It was pretty nice to show someone
around all the Rocktown #choss.

Thanks for the photo [Lee](http://instagram.com/jleewalter). My mother **loves** it.

<figure>
	<img src="/images/posts/rocktown-me.jpg">
    <figcaption>Lee asked me to look pretty. Sorry.</figcaption>
</figure>


## Horse Pens 40.

My guests left Rocktown Saturday evening.  I woke up Sunday, did laundry bought
some grocieries and made the drive to HP40.  After four days on my skin was too
shot to climb that day, so I walked around and figured out the must do items for
the next day.

I *awoke* to the worst sound in the world.  **Rain**.  The morning offered
misty, mank conditions.  Not exactly what I wanted for learning how to climb on
some of the best slopers.

I checked the weather and it showed ice and rain for the next two days.  I
decided to cut my losses in the South East and start the long haul to Hueco for
the Rock Rodeo on the 14th of Febuary.

<figure class="half">
	<a href="/images/posts/hp40-01.jpg"><img src="/images/posts/hp40-01.jpg"></a>
	<a href="/images/posts/hp40-02.jpg"><img src="/images/posts/hp40-02.jpg"></a>
    <figcaption>horsepensss.</figcaption>
</figure>

