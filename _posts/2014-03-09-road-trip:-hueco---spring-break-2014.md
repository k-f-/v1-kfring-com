---
layout: post
title: "Road Trip: Hueco - Spring Break 2014"
modified: 2014-04-13
category: roadtrip
tags: hueco, bouldering
description: "sunny and sharp."
image:
  feature: feat/hueco.jpg
  credit: Kyle Fring 
  creditlink: http://kfring.com
---

> Over the month of March I made what I hope will end up being at the very least
pad humping, spot providing bipedal mouth breathers.

## South East Exodus

Tired of being cold and wet in the South East I moved West to Hueco Tanks.
Happily, along the way I managed to visit Roswell, New Mexico.  The
comical museums and alien artwork everywhere was worth the small detour.

## The Hueco Tanks

My arrival in El Paso, Texas coincided with this years Hueco Rock Rodeo.  I had
previously agreed to volunteer for free room and board at the Alpine Club's Rock
Ranch.  

The Rodeo is essentially a big party that happens to have a climbing competition
before hand.  Famous crushers were present and I thought that was kind of neat.
What I found more interesting though was just how many people came out to the
middle of the desert for this, to be frank, sad looking pile of rocks.

> It turns out my skepticism wasn't warranted.  

Access is only a slight issue in my opinion.  Like everything, if you're
proactive or determined it is not much of a hindrance.  Besides the climbing is
totally worth the effort.

My first problem at Hueco was Sherman's Sign of the Cross.  It went and I
remember thinking to myself "If that thing is
<strong>only</strong> V3 I would need to prepare myself for the possibility I
would not be breaking any barriers here.".  Classic Kyle brand negative thinking.

## 28 Days Later (moar or less)

I didn't end up breaking into new grades at Hueco.
My friend [Swiss](http://www.earthsurfercinema.com/blog/2014/3/21/hueco) does a
better job of explaining the myriad of potential reasons for this.

Over the month of March I made what I hope will end up being at the very least
pad humping, spot providing bipedal mouth breathers.  At most, life long
friends.

Either way, I left physically much, much stronger and hugely psyched for
Sandstone at Joe's Valley.

## Photos

<figure>
	<a href="/images/posts/hueco-2.jpg"><img src="/images/posts/hueco-2.jpg"></a>
    <figcaption>The Terminator. Think Joshua Tree's Pig Pen on steroids.</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-3.jpg"><img src="/images/posts/hueco-3.jpg"></a>
    <figcaption>Zach on Dean's Journey - V10.</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-4.jpg"><img src="/images/posts/hueco-4.jpg"></a>
    <figcaption>Tomer showing the Grrr face on Crash Dummy - V7</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-5.jpg"><img src="/images/posts/hueco-5.jpg"></a>
    <figcaption>Nikias killing time waiting in line.</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-6.jpg"><img src="/images/posts/hueco-6.jpg"></a>
    <figcaption>Trying to dyno poorly on Bloody Flapper.
    Thanks <a href="http://www.earthsurfercinema.com">Swiss</a> for the photo.</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-7.jpg"><img src="/images/posts/hueco-7.jpg"></a>
    <figcaption>Bearclaw on Daily Dick Dose - V7</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-8.jpg"><img src="/images/posts/hueco-8.jpg"></a>
    <figcaption>Tomer again with that Grrr face on Mangum - V9</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-9.jpg"><img src="/images/posts/hueco-9.jpg"></a>
    <figcaption>Leanna Lockhart getting Mangum done - V9.</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-10.jpg"><img src="/images/posts/hueco-10.jpg"></a>
    <figcaption>Craig reaching for a hidden edge on Whispers of Mortality - V10</figcaption>
</figure>

<figure>
	<a href="/images/posts/hueco-1.jpg"><img src="/images/posts/hueco-1.jpg"></a>
    <figcaption>the desert always wins.</figcaption>
</figure>

